<?php

namespace CckSso\Console\Commands;

use Illuminate\Console\Command;
use CckSso\Services\SsoService;

/**
 * 创建SSO客户端
 */
class CreateSsoClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sso:create {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建SSO客户端';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name') ?: $this->ask('输入需要使用SSO服务的客户端（项目）名称');

        $service = new SsoService();
        $response = $service->createClient($name);
        if (isset($response['client_id'])) {
            $this->info('客户端创建成功');
            $this->table(['name', 'client_id', 'client_secret'], [ [
                $response['client_name'], $response['client_id'], $response['client_secret']
            ] ]);
        } else {
            $this->info('客户端创建失败');
        }
    }
}
