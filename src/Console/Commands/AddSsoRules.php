<?php

namespace CckSso\Console\Commands;

use Illuminate\Console\Command;

/**
 * 添加SSO相关的路由规则
 */
class AddSsoRules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sso:addrules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '添加SSO相关的路由规则';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rules = file_get_contents(__DIR__.'/../../rules/partner.txt');
        
        file_put_contents(base_path('routes/web.php'), $rules, FILE_APPEND);
        
        $this->info('SSO 第三方路由规则 已追加到 web.php');
    }
}
