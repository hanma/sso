<?php

/**
 * CCK SSO配置文件
 */
return [
    
    /* SSO客户端认证密钥 */
    'client_id' => '',
    'client_secret' => '',
    
    /* SSO服务端API地址 */
    'server_uri' => 'http://caicaikan.loc',
    'server_apis' => [
        'client_create' => '/api/sso/client/create', // 申请创建SSO客户端
        'user_register' => '/api/sso/user/register', // 用户注册
        'user_login' => '/api/sso/user/login', // 用户登录
        'user_logout' => '/api/sso/user/logout', // 用户登出
        'user_setinfo' => '/api/sso/user/setinfo', // 用户设置信息
        'user_newticket' => '/api/sso/user/refreshticket', // 用户刷新登录凭证
        'user_login_partner' => '/api/sso/user/loginpartner', // 用户第三方登录
    ],
    
    /* 第三方配置 */
    'partners' => [
        // 腾讯QQ
        'qq' => [
            'client_id' => 'your-app-id',
            'client_secret' => 'your-app-secret',
            'redirect' => 'http://localhost/callback.php',
        ],
        
        // 微信
        'wechat' => [
            'client_id' => 'your-app-id',
            'client_secret' => 'your-app-secret',
            'redirect' => 'http://localhost/callback.php',
        ],
        
        // 新浪微博
        'weibo' => [
            'client_id' => 'your-app-id',
            'client_secret' => 'your-app-secret',
            'redirect' => 'http://localhost/callback.php',
        ],
    ],

];