<?php

namespace CckSso\Services;

use GuzzleHttp\Client;

/**
 * SSO数据服务仓库
 */
class SsoService 
{
    /**
     * @var object HTTP请求类 
     */
    private $http;
    
    /**
     * @var array 存放客户端认证信息
     */
    private $auth;
    
    /**
     * @var array 存放SSO配置
     */
    private $config;

    /**
     * 初始化
     */
    public function __construct() 
    {
        $this->config = config('sso');
        
        $this->http = new Client([
            'base_uri' => $this->config['server_uri'],
            'headers' => [
                'User-Agent' => 'CaiCaiKan SSO V1.0',
            ],
        ]);
        
        $this->auth = [
            'client_id' => $this->config['client_id'],
            'client_secret' => $this->config['client_secret'],
        ];
    }
    
    /**
     * 创建客户端
     * 
     * @param string $name 客户端名称(项目名)
     * @return array
     */
    public function createClient($name)
    {
        $data = ['name' => $name];
        $response = $this->http->request('POST', $this->config['server_apis']['client_create'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }

    /**
     * 注册
     * 
     * @param int $phone 手机号码
     * @param string $password 密码,有就传【非必须】
     * @return array
     */
    public function register($phone, $password = null)
    {
        $data = $this->auth;
        $data['phone'] = $phone;
        $data['password'] = $password;

        $response = $this->http->request('POST', $this->config['server_apis']['user_register'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * 登录
     * 
     * @param int $phone 手机号码
     * @param string $password 密码
     * @return array
     */
    public function login($phone, $password)
    {
        $data = $this->auth;
        $data['phone'] = $phone;
        $data['password'] = $password;

        $response = $this->http->request('POST', $this->config['server_apis']['user_login'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * 登出
     * 
     * @param string $ticket 已登录的凭证
     * @return array
     */
    public function logout($ticket)
    {
        $data = $this->auth;
        $data['ticket'] = $ticket;
        $response = $this->http->request('POST', $this->config['server_apis']['user_logout'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * 第三方登录
     * 
     * @param string $type 第三方类型 (qq, wechat, weibo)
     * @param string $openid 第三方唯一标识
     * @param string | null 第三方头像
     * @param string | null 第三方昵称
     * @return array
     */
    public function loginByPartner($type, $openid, $headpic = null, $nickname = null)
    {
        $data = $this->auth;
        $data['type'] = $type;
        $data['openid'] = $openid;
        $data['headpic'] = $headpic;
        $data['nickname'] = $nickname;
        $response = $this->http->request('POST', $this->config['server_apis']['user_login_partner'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * 设置用户信息
     * 
     * @param string $ticket 已登录的凭证
     * @param array $info 用户信息
     * @example $info = [
     *       'phone' => '手机号码',
     *       'password' => '密码',
     *       'head_pic' => '头像地址',
     *       'nick_name' => '昵称',
     *       'real_name' => '真实姓名',
     *       'id_card' => '身份证号码',
     *       'is_verify' => '是否已实名认证(1:是, 0:否)',
     *       'qq_openid' => 'QQ第三方唯一标识',
     *       'wechat_openid' => '微信第三方唯一标识',
     *       'weibo_openid' => 'sina微博第三方唯一标识',
     *   ];
     * @return array
     */
    public function setInfo($ticket, array $info)
    {
        $data = $this->auth;
        $data['ticket'] = $ticket;
        $data += $info;
        $response = $this->http->request('POST', $this->config['server_apis']['user_setinfo'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * 刷新登录凭证
     * 
     * @param string $ticket 登录凭证
     */
    public function refreshTicket($ticket)
    {
        $data = $this->auth;
        $data['ticket'] = $ticket;
        $response = $this->http->request('POST', $this->config['server_apis']['user_newticket'], [
            'form_params' => $data,
        ]);
        $response = $this->parseResponse($response);
        
        return $response;
    }
    
    /**
     * 解析响应的结果
     * 
     * @param object $response
     * @return array
     */
    protected function parseResponse($response)
    {
        $response = json_decode((string) $response->getBody(), true);
        
        return $response;
    }
    
}
