<?php

namespace CckSso\Providers;

use Illuminate\Support\ServiceProvider;
use CckSso\Console\Commands\CreateSsoClient;
use CckSso\Console\Commands\AddSsoRules;

/**
 * SSO服务提供者
 */
class SsoServiceProvider extends ServiceProvider
{
    /**
     * 注册
     */
    public function register()
    {
        $this->publishes([__DIR__ . '/../config/sso.php' => config_path('sso.php')]);
        
        $this->commands([
            CreateSsoClient::class, // 创建客户端
            AddSsoRules::class, // 添加路由规则
        ]);
    }

    /**
     * 启动
     */
    public function boot()
    {
        
    }
}
