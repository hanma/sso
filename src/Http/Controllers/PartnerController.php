<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CckSso\Http\Controllers;

use Illuminate\Routing\Controller;
use CckSso\Services\PartnerService;

/**
 * 第三方相关的控制器类
 */
class PartnerController extends Controller
{
    /**
     * @var object 第三方服务对象
     */
    private $partnerService;
    
    /**
     * 初始化
     */
    public function __construct() 
    {
        $this->partnerService = new PartnerService();
    }
    
    /**
     * QQ登录
     */
    public function qq()
    {
        return $this->partnerService->redirect('qq');
    }
    /**
     * QQ登录回调
     */
    public function qqCallback()
    {
        $this->partnerService->callback('qq');
    }
    
    /**
     * 微信登录
     */
    public function weixin()
    {
        return $this->partnerService->redirect('wechat');
    }
    /**
     * 微信登录回调
     */
    public function weixinCallback()
    {
        $this->partnerService->callback('wechat');
    }
    
    
    /**
     * 微博登录
     */
    public function weibo()
    {
        return $this->partnerService->redirect('weibo');
    }
    /**
     * 微博登录回调
     */
    public function weiboCallback()
    {
        $this->partnerService->callback('weibo');
    }
    
}
