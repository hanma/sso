统一用户中心SSO客户端使用手册

STEP1:
composer require caicaikan/sso -vvv

STEP2:
php artisan vendor:publish --provider="CckSso\Providers\SsoServiceProvider"

STEP3:
# 创建SSO客户端
php artisan sso:create 客户端名称

# 追加SSO的路由规则到 rules/web.php
php artisan sso:addrules